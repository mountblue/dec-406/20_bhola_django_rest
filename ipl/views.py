from django.shortcuts import render
from .models import Matches, Deliveries
from ipl.serializers import MatchSerializer, DeliverySerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt


# home view
def home(request):
    return render(request, 'ipl/home.html')



