from django.db import models


# Create your models here.
class Matches(models.Model):
    id = models.AutoField(primary_key=True)  # id
    season = models.IntegerField()  # season
    city = models.CharField(max_length=100)  # city
    date = models.CharField(max_length=12)  # date
    team1 = models.CharField(max_length=100)  # team1
    team2 = models.CharField(max_length=100)  # team2
    toss_winner = models.CharField(max_length=100)  # toss_winner
    toss_decision = models.CharField(max_length=100)  # toss_decision
    result = models.CharField(max_length=100)  # result
    dl_applied = models.IntegerField()  # dl_applied
    winner = models.CharField(max_length=100)  # winner
    win_by_runs = models.IntegerField()  # win_by_runs
    win_by_wickets = models.IntegerField()  # win_by_wickets
    player_of_match = models.CharField(max_length=100)  # player_of_match
    venue = models.CharField(max_length=100)  # venue
    umpire1 = models.CharField(max_length=100)  # umpire1
    umpire2 = models.CharField(max_length=100)  # umpire2
    umpire3 = models.CharField(max_length=100)  # umpire3

    def __str__(self):
        return str(self.id)


class Deliveries(models.Model):
    match_id = models.ForeignKey(Matches, on_delete=models.CASCADE)  # match_id
    inning = models.IntegerField()  # inning
    batting_team = models.CharField(max_length=100)  # batting_team
    bowling_team = models.CharField(max_length=100)  # bowling_team
    overs = models.IntegerField()  # overs
    ball = models.IntegerField()  # ball
    batsman = models.CharField(max_length=100)  # batsman
    non_striker = models.CharField(max_length=100)  # non_striker
    bowler = models.CharField(max_length=100)  # bowler
    is_super_over = models.IntegerField()  # is_super_over
    wide_runs = models.IntegerField()  # wide_runs
    bye_runs = models.IntegerField()  # bye_runs
    legbye_runs = models.IntegerField()  # legbye_runs
    noball_runs = models.IntegerField()  # noball_runs
    penalty_runs = models.IntegerField()  # penalty_runs
    batsman_runs = models.IntegerField()  # batsman_runs
    extra_runs = models.IntegerField()  # extra_runs
    total_runs = models.IntegerField()  # total_runs
    player_dismissed = models.CharField(max_length=100)  # player_dismissed
    dismissal_kind = models.CharField(max_length=100)  # dismissal_kind
    fielder = models.CharField(max_length=100)  # fielder

    def __str__(self):
        return str(self.id) + " : " +\
            str(self.match_id_id) + " : " + str(self.inning)
